import Config

config :kaffe,
  consumer: [
    endpoints: [localhost: 9092],
    topics: ["testtopic"],
    consumer_group: "hello-consumer-group",
    message_handler: MessageProcessor,
    offset_reset_policy: :reset_to_latest,
    max_bytes: 500_000,
    worker_allocation_strategy: :worker_per_topic_partition,
    start_with_earliest_message: true # default false
  ],
  producer: [
    endpoints: [localhost: 9092], # [hostname: port]
    topics: ["testtopic"]
  ]
