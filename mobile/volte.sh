#!/bin/sh

# https://wiki.postmarketos.org/wiki/PINE64_PinePhone_(pine64-pinephone)#VoLTE

# List of embedded VoLTE profiles
echo 'AT+QMBNCFG="list"' | sudo atinout - /dev/ttyUSB2 - | grep ROW_Generic_3GPP

# Select the profile most applicable to your provider, or use the Generic one if nothing matches
echo 'AT+QMBNCFG="select","ROW_Generic_3GPP"' | sudo atinout - /dev/ttyUSB2 -

# Enable calling over ip (VoLTE)
echo 'AT+QCFG="ims",1' | sudo atinout - /dev/ttyUSB2 -

# Reboot the modem to apply the settings
echo 'AT+CFUN=1,1' | sudo atinout - /dev/ttyUSB2 -

echo 'AT+CLCC' | sudo atinout - /dev/ttyUSB2 -
